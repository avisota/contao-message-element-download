<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:02:06+01:00
 */

$GLOBALS['TL_LANG']['MCE']['download']['0'] = 'Telechargiada';
$GLOBALS['TL_LANG']['MCE']['download']['1'] = 'Generescha ina colliaziun per telechargiar ina daoteca.';
