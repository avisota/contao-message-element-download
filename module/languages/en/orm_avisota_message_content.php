<?php

/**
 * Avisota newsletter and mailing system
 * Copyright (C) 2013 Tristan Lins
 *
 * PHP version 5
 *
 * @copyright  bit3 UG 2013
 * @author     Tristan Lins <tristan.lins@bit3.de>
 * @package    avisota/contao-message-element-download
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadSource'] = array(
    'File',
    'Chose the download file.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadTitle']  = array(
    'Title',
    'Define a custom download title.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['download_legend'] = 'Download';
