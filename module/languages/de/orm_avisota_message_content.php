<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-29T04:01:54+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadSource']['0'] = 'Datei';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadSource']['1'] = 'Bitte wählen Sie eine Download-Datei aus.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadTitle']['0']  = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['downloadTitle']['1']  = 'Legen Sie einen individuellen Titel für die Datei fest.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['download_legend']     = 'Download';
